function layThongTinForm() {
  var maSV = document.getElementById("txtMaSV").value;
  var tenSV = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var diemToan = document.getElementById("txtDiemToan").value;
  var diemLy = document.getElementById("txtDiemLy").value;
  var diemHoa = document.getElementById("txtDiemHoa").value;
  return new SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa);
}
function xoaThongTinForm() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("spanMaSV").innerText = "";
  document.getElementById("spanTenSV").innerText = "";
  document.getElementById("spanEmailSV").innerText = "";
  document.getElementById("spanMatKhau").innerText = "";
  document.getElementById("spanToan").innerText = "";
  document.getElementById("spanLy").innerText = "";
  document.getElementById("spanHoa").innerText = "";
}
function createSvTR(sv) {
  var newSvTR = document.createElement("tr");
  newSvTR.innerHTML = `<td>${sv.maSV}</td>
  <td>${sv.tenSV}</td>
  <td>${sv.email}</td>
  <td>${sv.diemTB()}</td>
  <td>
  <button class="btn btn-danger" onclick="xoaSinhVien('${
    sv.maSV
  }')">Xóa</button>
  <button class="btn btn-warning" onclick="suaSinhVien('${
    sv.maSV
  }')">Sửa</button>
  </td>`;
  return newSvTR;
}
function renderDSSV(svArr) {
  const tbodySV = document.getElementById("tbodySinhVien");
  tbodySV.innerHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var newSvTR = createSvTR(svArr[i]);
    tbodySV.append(newSvTR);
  }
}
function timKiemViTri(id, dssv) {
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.maSV == id) {
      return i;
    }
  }
  return -1;
}
function showThongTinForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSV;
  document.getElementById("txtTenSV").value = sv.tenSV;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
  document.getElementById("txtMaSV").disabled = true;
}
function checkMaSV(sv, dssv) {
  var isValid =
    validation.kiemTraRong(sv.maSV, "spanMaSV", "Mã SV") &&
    validation.kiemTraDoDai(
      sv.maSV,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 ký tự",
      4,
      4
    ) &&
    validation.checkDuplicate(
      sv.maSV,
      dssv,
      "spanMaSV",
      "Mã Sinh Viên này đã được sử dụng, vui lòng chọn mã khác"
    );
  return isValid;
}
function checkFormValidationWithoutMaSV(sv) {
  var isValid =
    validation.kiemTraRong(sv.tenSV, "spanTenSV", "Tên SV") &&
    validation.kiemTraTen(
      sv.tenSV,
      "spanTenSV",
      "Tên không bao gồm số và ký tự đặc biệt"
    );

  isValid &=
    validation.kiemTraRong(sv.email, "spanEmailSV", "Email") &&
    validation.kiemTraEmail(sv.email, "spanEmailSV", "Email sai định dạng");

  isValid &=
    validation.kiemTraRong(sv.matKhau, "spanMatKhau", "Mật khẩu") &&
    validation.checkPassword(
      sv.matKhau,
      "spanMatKhau",
      "Mật khẩu phải bao gồm 1 ký tự viết Hoa, 1 ký tự thường, 1 ký số và 1 ký tự đặc biệt"
    );

  isValid &=
    validation.kiemTraRong(sv.diemToan, "spanToan", "Điểm Toán") &&
    validation.kiemTraSo(sv.diemToan, "spanToan", "Điểm Toán") &&
    validation.kiemTraGiaTri(sv.diemToan * 1, "spanToan", "Điểm Toán", 0, 10);

  isValid &=
    validation.kiemTraRong(sv.diemLy, "spanLy", "Điểm Lý") &&
    validation.kiemTraSo(sv.diemLy, "spanLy", "Điểm Lý") &&
    validation.kiemTraGiaTri(sv.diemLy * 1, "spanLy", "Điểm Lý", 0, 10);

  isValid &=
    validation.kiemTraRong(sv.diemHoa, "spanHoa", "Điểm Hóa") &&
    validation.kiemTraSo(sv.diemHoa, "spanHoa", "Điểm Hóa") &&
    validation.kiemTraGiaTri(sv.diemHoa * 1, "spanHoa", "Điểm Hóa", 0, 10);
  return isValid;
}
