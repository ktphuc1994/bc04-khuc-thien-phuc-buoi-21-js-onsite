// START Get DSSV from Local Storage
var dssv = [];
const DSSV_LOCALSTORAGE = "DSSV";
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE); //lấy thông tin từ Local Storage
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    dssv[i] = new SinhVien(
      sv.maSV,
      sv.tenSV,
      sv.email,
      sv.matKhau,
      sv.diemToan,
      sv.diemLy,
      sv.diemHoa
    );
  }
  renderDSSV(dssv);
}
// END Get DSSV from Local Storage

// START Thêm Sinh Viên
function themSV() {
  var newSV = layThongTinForm();
  //   console.log("newSV: ", newSV);
  var isValid = checkMaSV(newSV, dssv);
  isValid &= checkFormValidationWithoutMaSV(newSV);
  if (isValid) {
    dssv.push(newSV);
    // console.log("dssv: ", dssv);
    var dssvJSON = JSON.stringify(dssv); //tạo JSON
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJSON); //lưu JSON vào Local Storage
    document.getElementById("tbodySinhVien").append(createSvTR(newSV));
    xoaThongTinForm();
  }
}
// END Thêm Sinh Viên

// START Xóa Sinh Viên
function xoaSinhVien(maSV) {
  var index = timKiemViTri(maSV, dssv);
  if (index != -1) {
    dssv.splice(index, 1);
    const tbodySV = document.getElementById("tbodySinhVien");
    tbodySV.removeChild(tbodySV.children[index]);
    localStorage.setItem(DSSV_LOCALSTORAGE, JSON.stringify(dssv));
  }
}
// END Xóa Sinh Viên

// START Sửa Sinh Viên
function suaSinhVien(maSV) {
  var index = timKiemViTri(maSV, dssv);
  // console.log("index: ", index);
  if (index != -1) {
    showThongTinForm(dssv[index]);
    document.getElementById("btnThemSV").style.display = "none";
    document.getElementById("btnCapNhatSV").style.display = "inline-block";
  }
}
// END Sửa Sinh Viên

// START Cập nhật thông tin Sinh Viên
function capNhatThongTinForm() {
  var updatedSV = layThongTinForm();
  var isValid = checkFormValidationWithoutMaSV(updatedSV);
  if (isValid) {
    var maSV = updatedSV.maSV;
    var index = timKiemViTri(maSV, dssv);
    if (index != -1) {
      dssv[index] = updatedSV;
      localStorage.setItem(DSSV_LOCALSTORAGE, JSON.stringify(dssv));
      document
        .getElementById("tbodySinhVien")
        .children[index].replaceWith(createSvTR(updatedSV));
      xoaThongTinForm();
      document.getElementById("txtMaSV").disabled = false;
      document.getElementById("btnThemSV").style.display = "inline-block";
      document.getElementById("btnCapNhatSV").style.display = "none";
    }
  }
}
// END Cập nhật thông tin Sinh Viên

// START Reset form nhập thông tin
function resetDSSV() {
  xoaThongTinForm();
  document.getElementById("txtMaSV").disabled = false;
  document.getElementById("btnThemSV").style.display = "inline-block";
  document.getElementById("btnCapNhatSV").style.display = "none";
}
// END Reset form nhập thông tin

// START Enter to Search
document
  .getElementById("txtSearch")
  .addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      event.preventDefault;
      document.getElementById("btnSearch").click();
    }
  });
// END Enter to Search

// START Tìm kiếm Sinh Viên
document.getElementById("btnSearch").onclick = function () {
  var tenSinhVien = document.getElementById("txtSearch").value;
  var DssvTheoTen = dssv.filter(function (sv) {
    return sv.tenSV.search(tenSinhVien) != -1;
  });
  renderDSSV(DssvTheoTen);
};
// END Tìm kiếm Sinh Viên
