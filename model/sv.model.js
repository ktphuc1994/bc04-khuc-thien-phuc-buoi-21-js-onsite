function SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa) {
  this.maSV = maSV;
  this.tenSV = tenSV;
  this.email = email;
  this.matKhau = matKhau;
  this.diemToan = diemToan;
  this.diemLy = diemLy;
  this.diemHoa = diemHoa;
  this.diemTB = function () {
    return (
      Math.round(((diemToan * 1 + diemLy * 1 + diemHoa * 1) / 3) * 100) / 100
    );
  };
}
