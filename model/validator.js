var validation = {
  kiemTraRong: function (value, idError, subject) {
    if (value.length == 0) {
      document.getElementById(
        idError
      ).innerText = `${subject} không được để rỗng`;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraGiaTri: function (value, idError, subject, min, max) {
    if (value < min || value > max) {
      document.getElementById(
        idError
      ).innerText = `${subject} phải nằm trong khoảng từ ${min} đến ${max}`;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraEmail: function (value, idError, message) {
    const emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (emailFormat.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  kiemTraSo: function (value, idError, subject) {
    const numbers = /^\d+$/;
    if (value.match(numbers)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = `${subject} phải là số`;
      return false;
    }
  },
  kiemTraTen: function (value, idError, message) {
    const letters =
      /^[A-Za-zỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐ' ]+$/;
    if (value.match(letters)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  checkDuplicate: function (value, dssv, idError, message) {
    var index = timKiemViTri(value, dssv);
    if (index == -1) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  checkPassword: function (value, idError, message) {
    const matKhauFormat =
      /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[@$!%*#?&])([a-zA-Z0-9@$!%*#?&]){4,}$/;
    if (value.match(matKhauFormat)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
};
